#include <cmath>
#include <iostream>
#include <random>
#include <string>
#include <vector>
#include <algorithm>

#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_scancode.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_ttf.h>

#define ACCEL 40.0f
#define GRAVITY 2.5f

#define MAX_LIVES 400.0f

#define DINO "img/dino.bmp"
#define DINO_ASPECT_RATIO (64.0f/ 45.0f)
#define DINO_SIZE (144.0f / 3.0f)

#define FLOOR "img/floor.bmp"
#define FLOOR_H (6.0f / 8.0f)
#define FLOOR_HIT (7.0f / 8.0f)
#define FLOOR_SPEED 21.25f
#define OBSTACLE_ROCK "img/rock.bmp"
#define OBSTACLE_TREE "img/tree.bmp"
#define OBSTACLE_STAR "img/star.bmp"

#define TREE_CHANCE 0.35f
#define STAR_CHANCE 0.15f

#define ROCK_DAMAGE 3.0f
#define TREE_DAMAGE 5.0f
#define STAR_DAMAGE -2.0f

#define ROCK_W (1.0f / 24.0f)
#define ROCK_H (1.0f / 12.0f)
#define TREE_W (1.0f / 19.20f)
#define TREE_H (1.0f / 6.0f)
#define STAR_W (1.0f / 29.2f)
#define STAR_H (1.0f / 29.2f)

#define SPAWN_CHANCE 0.08

#define SCORE_INC 8000.0f

struct obstacle {
    float X, w, h, damage;
    SDL_Texture* texture;
};

bool isKeyPressed(SDL_Scancode scancode) {
    const Uint8* keyboardState = SDL_GetKeyboardState(nullptr);
    return keyboardState[scancode];
}

bool touching(float x1, float y1, float w1, float h1, float x2, float y2, float w2, float h2) {
    return !(x1 + w1 <= x2 || x2 + w2 <= x1 || y1 + h1 <= y2 || y2 + h2 <= y1);
}

void renderRect(SDL_Renderer* renderer, float x, float y, float w, float h) {
    SDL_RenderDrawLine(renderer, x, y, x + w, y);
    SDL_RenderDrawLine(renderer, x + w, y, x + w, y + h);
    SDL_RenderDrawLine(renderer, x, y, x, y + h);
    SDL_RenderDrawLine(renderer, x, y + h, x + w, y + h);
}

int main(void) {
    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window* window = SDL_CreateWindow("(Not Chrome) Dino", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1920, 1080, SDL_WINDOW_RESIZABLE);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    TTF_Init();
    TTF_Font* font = TTF_OpenFont("default.ttf", 24);

    int score = 0;

    bool quit = false;
    SDL_Event event;

    float lives = MAX_LIVES;

    std::random_device rd;
    std::mt19937 mt(rd());

    SDL_Texture* dino_texure = IMG_LoadTexture(renderer, DINO);
    if(dino_texure == NULL) {
        std::cerr<<"Failed to load texture "<<DINO<<std::endl;
        return 1;
    }

    SDL_Texture* floor_texture = IMG_LoadTexture(renderer, FLOOR);
    if(floor_texture == NULL) {
        std::cerr<<"Failed to load texture "<<FLOOR<<std::endl;
        return 1;
    }

    SDL_Texture* rock_texture = IMG_LoadTexture(renderer, OBSTACLE_ROCK);
    if(rock_texture == NULL) {
        std::cerr<<"Failed to load texture "<<OBSTACLE_ROCK<<std::endl;
        return 1;
    }

    SDL_Texture* tree_texture = IMG_LoadTexture(renderer, OBSTACLE_TREE);
    if(tree_texture == NULL) {
        std::cerr<<"Failed to load texture "<<OBSTACLE_TREE<<std::endl;
        return 1;
    }

    SDL_Texture* star_texture = IMG_LoadTexture(renderer, OBSTACLE_STAR);
    if(star_texture == NULL) {
        std::cerr<<"Failed to load texture "<<OBSTACLE_STAR<<std::endl;
        return 1;
    }

    float height = 0, vspeed = 0;
    float floorpos = 0;
    bool debug = 0;

    std::vector<obstacle> obstacles;

    Uint32 prevTime = SDL_GetTicks();
    Uint32 currentTime, frameTime;
    int frameCount = 0;
    double fps = 0.0;

    while (!quit) {
        int windowWidth, windowHeight;
        SDL_GetWindowSize(window, &windowWidth, &windowHeight);

        score++;

        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = true;
            } else if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
                SDL_GetRendererOutputSize(renderer, &event.window.data1, &event.window.data2);
            } else if (event.type == SDL_KEYDOWN) {
                if(event.key.keysym.sym == SDLK_b) {
                    debug ^= 1;
                }
            }
        }

        if (isKeyPressed(SDL_SCANCODE_SPACE) && height <= 0.0f) {
            vspeed = ACCEL;
        }

        if(height > 0 || vspeed > 0) {
            height += vspeed;
            vspeed -= GRAVITY;
        }

        SDL_SetRenderDrawColor(renderer, 10, 10, 32, 0);
        SDL_RenderClear(renderer);

        float dino_square = (windowWidth * windowHeight) / DINO_SIZE;
        float dino_w = sqrt(dino_square / DINO_ASPECT_RATIO);
        float dino_h = dino_w * DINO_ASPECT_RATIO;

        SDL_Rect dino_frame;
        dino_frame.x = windowWidth / 4;
        dino_frame.y = windowHeight * FLOOR_HIT - dino_h - height;
        dino_frame.w = dino_w;
        dino_frame.h = dino_h;

        SDL_SetRenderDrawColor(renderer, lives <= MAX_LIVES ? (1 - (lives / MAX_LIVES)) * 255 : 0, lives <= MAX_LIVES ? (lives / MAX_LIVES) * 255 : 0,
                lives <= MAX_LIVES ? 0 : (1 - ((lives <= MAX_LIVES * 2 ? lives : MAX_LIVES * 2 - MAX_LIVES) / MAX_LIVES)) * 255, 0);
        SDL_Rect HP;
        HP.x = 0;
        HP.y = 0;
        HP.w = lives;
        HP.h = 20;
        SDL_RenderFillRect(renderer, &HP);

        floorpos += FLOOR_SPEED;
        if(floorpos >= windowWidth * 2) floorpos = 0;

        SDL_Rect floor_frame_p0;
        floor_frame_p0.x = 0 - floorpos;
        floor_frame_p0.y = windowHeight * FLOOR_H;
        floor_frame_p0.w = windowWidth * 2;
        floor_frame_p0.h = windowHeight * (1 - FLOOR_H);
        SDL_Rect floor_frame_p1;
        floor_frame_p1.x = windowWidth * 2 - floorpos;
        floor_frame_p1.y = windowHeight * FLOOR_H;
        floor_frame_p1.w = windowWidth * 2;
        floor_frame_p1.h = windowHeight * (1 - FLOOR_H);

        SDL_RenderCopy(renderer, floor_texture, NULL, &floor_frame_p0);
        SDL_RenderCopy(renderer, floor_texture, NULL, &floor_frame_p1);

        if(score % 10 == 0) {
            std::uniform_real_distribution<double> spawn(0, 1);
            if(spawn(mt) <= SPAWN_CHANCE + (900 / SCORE_INC)) {
                obstacle ouch;
                ouch.X = windowWidth;

                std::uniform_real_distribution<double> type(0, 1);
                double o_type = type(mt);
                if(o_type <= STAR_CHANCE) {
                    ouch.texture = star_texture;
                    ouch.damage = STAR_DAMAGE;
                    ouch.w = windowWidth * STAR_W;
                    ouch.h = windowHeight * STAR_H;
                } else if (o_type <= TREE_CHANCE) {
                    ouch.texture = tree_texture;
                    ouch.damage = TREE_DAMAGE;
                    ouch.w = windowWidth * TREE_W;
                    ouch.h = windowHeight * TREE_H;
                } else {
                    ouch.texture = rock_texture;
                    ouch.damage = ROCK_DAMAGE;
                    ouch.w = windowWidth * ROCK_W;
                    ouch.h = windowHeight * ROCK_H;
                }

                obstacles.push_back(ouch);
            }
        }

        for(obstacle& ouch : obstacles) {
            SDL_Rect obstacle_frame;
            obstacle_frame.x = ouch.X;
            obstacle_frame.y = windowHeight * FLOOR_HIT - ouch.h;
            obstacle_frame.w = ouch.w;
            obstacle_frame.h = ouch.h;

            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);
            if(touching(dino_frame.x, dino_frame.y, dino_frame.w, dino_frame.h, obstacle_frame.x, obstacle_frame.y, obstacle_frame.w, obstacle_frame.h)) {
                SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);
                lives -= ouch.damage;
            }
            if(debug) renderRect(renderer, obstacle_frame.x, obstacle_frame.y, obstacle_frame.w, obstacle_frame.h);
            SDL_RenderCopy(renderer, ouch.texture, NULL, &obstacle_frame);
            ouch.X -= FLOOR_SPEED;
        }
        if(lives <= 0) {
            std::cout<<"U DED :)"<<std::endl<<"Ur score: "<<score<<std::endl;
            break;
        }

        obstacles.erase(std::remove_if(obstacles.begin(), obstacles.end(), [](const obstacle& x) {return x.X <= 0;}), obstacles.end());

        SDL_RenderCopy(renderer, dino_texure, NULL, &dino_frame);
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);
        if(debug) renderRect(renderer, dino_frame.x, dino_frame.y, dino_frame.w, dino_frame.h);


        frameCount++;
        
        if (frameCount % 60 == 0) {
            currentTime = SDL_GetTicks();
            frameTime = currentTime - prevTime;
            prevTime = currentTime;
            fps = frameCount / (frameTime / 1000.0);
            frameCount = 0;

        }
        if(debug) {
            SDL_Color fps_color = {255, static_cast<Uint8>(255 * fps / 60.0f), static_cast<Uint8>(255 * fps / 60.0f), 0};
            std::string fps_m = "FPS: " + std::to_string(static_cast<int>(fps));
            SDL_Surface* surface = TTF_RenderText_Solid(font, fps_m.c_str(), fps_color);
            SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);

            SDL_Rect fps_m_frame;
            fps_m_frame.x = windowWidth - 90;
            fps_m_frame.y = 0;
            fps_m_frame.w = 90;
            fps_m_frame.h = 36;

            SDL_RenderCopy(renderer, texture, NULL, &fps_m_frame);
            SDL_FreeSurface(surface);
        }

        SDL_RenderPresent(renderer);
    }

    TTF_CloseFont(font);
    TTF_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
