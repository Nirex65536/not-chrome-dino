# (Not Chrome) Dino
A game like chrome dino, but with (extremely ugly) colored textures

## Usage
 - Clone the repo
 - ```make run```

## Some Variables (defined in the only source file)
 - ACCEL:           Dino jump acceleration
 - GRAVITY:         Dino gravity
 - MAX_LIVES:       I think you should understand this
 - TREE_CHANCE:     Chance for an obstacle to be a tree instead of a rock
 - STAR_CHANCE:     Chance for an obstacle to be a star instead of a tree
 - ROCK_DAMAGE:     The damage a rock does to the dino per frame
 - TREE_DAMAGE:     The damage a tree does to the dino per frame
 - STAR_DAMAGE:     The damage a star does to the dino per frame
 - SPAWN_CHANCE:    The chance for an obstacle to spawn
 - SCORE_INC:       The actual spawn-chance is SPAWN_CHANCE + (score / SCORE_INC). The score is incremented every frame.
