TARGET=dino
CXX=g++
CXXFLAGS=-Wall -Werror -Wextra -O3 -pedantic -Iinclude -c -ggdb $(shell pkg-config --cflags sdl2 SDL2_image SDL2_ttf)
LDFLAGS=$(shell pkg-config --libs sdl2 SDL2_image SDL2_ttf)

SOURCES=$(shell find . -name "*.cpp")
OBJECTS=$(SOURCES:.cpp=.o)

.PHONY: clean all run

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CXX) $(LDFLAGS) $(OBJECTS) -o $(TARGET)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $^ -o $@

clean:
	rm -f $(OBJECTS) $(TARGET)

run: $(TARGET)
	./$(TARGET)
